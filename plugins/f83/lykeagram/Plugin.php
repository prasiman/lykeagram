<?php namespace F83\Lykeagram;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return [
        	'F83\Lykeagram\Components\UploadForm' => 'uploadForm'
        ];
    }

    public function registerSettings()
    {
    }
}
