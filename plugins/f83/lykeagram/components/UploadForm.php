<?php namespace F83\Lykeagram\Components;

use Cms\Classes\ComponentBase;
use Input;
use Validator;
use ValidationException;
use Redirect;
use F83\Lykeagram\Models\Post;
use Flash;
use System\Models\File;

class UploadForm extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Upload Form',
            'description' => 'For upload'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onSave(){

        $validator = Validator::make(
            $form = Input::all(),[

            ]
        );

        if($validator->fails()){
            throw new ValidationException($validator);
        }

        $post = new Post();
        $caption = Input::get('caption');
        $post->caption = $caption;
        $post->uploader = Input::get('uploader');
        $post->slug = $this->createSlug($caption, '-');
        $fileName = Input::file('image')->getClientOriginalName();
        Input::file('image')->move('storage/app/media/', $fileName);
        $post->image = $fileName;
        $post->save();
        Flash::success('New post added!');
        return Redirect::to('');
    }

    public function createSlug($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter,
        preg_replace('/[^A-Za-z0-9-]+/', $delimiter,
        preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }

    public function onImageUpload(){
        $image = Input::all();
        $file = (new File())->frompost($image['image']);

        return[
            '#imageResult' => '<img src="' . $file->getThumb(150,150,['mode' => 'crop']) .'">'
        ];
    }
}
