<?php namespace F83\Lykeagram\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePostsTable extends Migration
{
    public function up()
    {
        Schema::create('f83_lykeagram_posts', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('caption')->nullable();
            $table->string('image', 255);
            $table->string('uploader', 50);
            $table->string('slug', 20);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('f83_lykeagram_posts');
    }
}
