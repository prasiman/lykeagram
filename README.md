# Lyke-a-gram
*by F83*

---

Lyke-a-gram is an Instagram-like web app built on OctoberCMS.

## Prerequisites

1. PHP > 7.1
2. MySQL or MariaDB
3. [Composer](http://getcomposer.org)
4. [Yarn](https://yarnpkg.com)

## Getting Started

1. Clone to your base project directory.

	```
	git clone https://github.com/prasiman/lykeagram.git <project-name>
	```

2. Don't forget to remove `.git` folder, create your own repository.

	```
	rm -rf !$/.git
	```

3. Install composer dependencies.

	```
	composer install
	```

4. Create configuration file `.env` (copy from `.env.example`) and set the database configuration.

	```
	DB_HOST=localhost
	DB_DATABASE=<database-name>
	DB_USERNAME=<database-user>
	DB_PASSWORD=<database-password>
	```

5. Migrate October database.

	```
	php artisan october:up
	```

6. Install frontend library using Yarn. Go to theme directory first.

    ```
    cd themes/my-theme
	yarn install
	```

7. For security reason, please generate new application key.

	```
	php artisan key:generate
	```